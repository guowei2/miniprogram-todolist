// pages/todolist/todolist.ts
import { ComponentWithComputed } from 'miniprogram-computed'

interface TodoItem {
  id: number
  title: string
  completed: boolean
}

const STORAGE_KEY = 'vue-todomvc'

const filters = {
  // 解决计算属性联动更新问题，这里进行数据关联解绑
  all: (todos: TodoItem[]) => todos.map((v) => JSON.parse(JSON.stringify(v))),
  active: (todos: TodoItem[]) =>
    todos.map((v) => JSON.parse(JSON.stringify(v))).filter((todo) => !todo.completed),
  completed: (todos: TodoItem[]) =>
    todos.map((v) => JSON.parse(JSON.stringify(v))).filter((todo) => todo.completed)
}

ComponentWithComputed({
  /**
   * 页面的初始数据
   */
  data: {
    addText: '',
    todos: wx.getStorageSync<TodoItem[]>(STORAGE_KEY) || [],
    visibility: 'all' as keyof typeof filters,
    editedTodo: null as TodoItem | null
  },

  computed: {
    remaining(data) {
      return filters.active(data.todos).length
    },
    filteredTodos(data) {
      return filters[data.visibility](data.todos)
    }
  },

  watch: {
    todos(todos: TodoItem[]) {
      wx.setStorageSync<TodoItem[]>(STORAGE_KEY, todos)
    }
  },

  methods: {
    addTodo(): void {
      const value = this.data.addText.trim()
      if (value) {
        const addItem = {
          id: Date.now(),
          title: value,
          completed: false
        }
        this.setData({
          todos: [...this.data.todos, addItem],
          addText: ''
        })
      }
    },

    toggleAll() {
      const allChecked = this.data.todos.every((v) => v.completed)
      this.setData({
        todos: this.data.todos.map((v) => ({ ...v, completed: !allChecked }))
      })
    },

    editTodo(e: WechatMiniprogram.BaseEvent<{ index: number }>) {
      const todo = this.data.todos[e.currentTarget.dataset.index]
      this.setData({
        editedTodo: JSON.parse(JSON.stringify(todo))
      })
    },

    removeTodo(e: WechatMiniprogram.BaseEvent<{ index: number }>) {
      this.setData({
        todos: this.data.todos.filter((_, i) => i !== e.currentTarget.dataset.index)
      })
    },

    doneEdit(e: WechatMiniprogram.BaseEvent<{ index: number }>) {
      const title = this.data.editedTodo!.title.trim()
      if (title) {
        this.setData({
          [`todos[${e.currentTarget.dataset.index}].title`]: title
        })
      } else this.removeTodo(e)
      this.setData({
        editedTodo: null
      })
    },

    cancelEdit() {
      this.setData({
        editedTodo: null
      })
    },

    removeCompleted() {
      this.setData({
        todos: filters.active(this.data.todos)
      })
    },

    toggle(e: WechatMiniprogram.BaseEvent<{ index: number }>) {
      const index = e.currentTarget.dataset.index
      this.setData({
        [`todos[${index}].completed`]: !this.data.todos[index].completed
      })
    },

    onSwitch(e: WechatMiniprogram.BaseEvent<{ href: keyof typeof filters }>) {
      this.setData({
        visibility: e.currentTarget.dataset.href
      })
    },

    onEditChange(e: WechatMiniprogram.Input) {
      this.setData({
        'editedTodo.title': e.detail.value
      })
    }
  }
})
