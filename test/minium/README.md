# minium

使用[minium](https://minitest.weixin.qq.com/#/minium/Python/readme)进行测试

## 安装

需要 Python 3.8及以上

```shell
pip3 install minium
```

## 运行测试

```shell
minitest -m test.first_test -c config.json -g
```

## 查看结果

```shell
python3 -m http.server 12345 -d outputs
```
