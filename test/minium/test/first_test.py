#!/usr/bin/env python3
import minium
class FirstTest(minium.MiniTest):
    def test_get_system_info(self):
        sys_info = self.mini.get_system_info()
        self.assertIn("SDKVersion", sys_info)

    def __loop(self,fn,check):
        fn()
        self.page.wait_for(1)
        if check():
            return
        else:
            self.__loop(check,fn)
    def __add_text(self,text):
        self.page.get_element('.new-todo').input(text)
        self.page.wait_for(1)
        self.page.get_element('.add-btn').tap()
        self.page.wait_for(1)
        return text
    def test_visits(self):
        text = self.page.get_element('.todoapp>.header>.h1').inner_text
        self.assertEqual(text, 'Todos')
    def test_add_todo(self):
        text = self.__add_text('add 1')
        expectText = self.page.get_element('.todo-list>.todo:last-child>.view>label').inner_text
        self.assertEqual(expectText, text)
    def test_edit_todo(self):
        self.__add_text('add edit')
        editText = 'edit text'
        last = self.page.get_element('.todo-list>.todo:last-child')
        self.__loop(
            lambda: last.get_element('.view>label').long_press(),
            lambda: self.page.element_is_exists('.edit')
        )
        last.get_element('.edit').input(editText)
        self.page.wait_for(1)
        last.get_element('.confirm').tap()
        self.page.wait_for(1)
        expectText = last.get_element('.view>label').inner_text
        self.assertEqual(expectText, editText)
    def test_cancel_edit(self):
        self.__add_text('add cancel')
        last = self.page.get_element('.todo-list>.todo:last-child')
        oldText = last.get_element('.view>label').inner_text
        self.__loop(
            lambda: last.get_element('.view>label').long_press(),
            lambda: self.page.element_is_exists('.edit')
        )
        last.get_element('.edit').input('will cancel text')
        self.page.wait_for(1)
        last.get_element('.cancel').tap()
        self.page.wait_for(1)
        expectText = last.get_element('.view>label').inner_text
        self.assertEqual(expectText, oldText)
    def test_delete_todo(self):
        self.__add_text('add delete')
        LengthBefore = len(self.page.get_elements('.todo-list>.todo'))
        self.page.get_element('.todo-list>.todo:last-child .destroy').tap()
        self.page.wait_for(1)
        LengthAfter = len(self.page.get_elements('.todo-list>.todo'))
        self.assertEqual(LengthBefore - LengthAfter, 1)
