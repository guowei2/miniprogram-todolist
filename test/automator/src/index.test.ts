import automator from 'miniprogram-automator'
import { describe, expect, beforeAll, it, afterAll } from '@jest/globals'
import Page from 'miniprogram-automator/out/Page'
import Element from 'miniprogram-automator/out/Element'
import MiniProgram from 'miniprogram-automator/out/MiniProgram'

type ElementExt = Element & { input(value: string): Promise<void> }

describe('pages/index/index', () => {
  let program: MiniProgram
  let page: Page
  beforeAll(async () => {
    program = await automator.launch({
      cliPath: 'D:\\Program Files\\微信web开发者工具\\cli.bat', // 工具 cli 位置，如果你没有更改过默认安装位置，可以忽略此项
      projectPath: 'D:\\workplace\\person\\miniprogram-todolist' // 项目文件地址
    })

    // 重新reLaunch至首页，并获取首页page对象（其中 program 是uni-automator自动注入的全局对象）
    page = (await program.reLaunch('/pages/todolist/todolist')) as Page
    await page.waitFor('.new-todo')
  })

  it('visits the todolist', async () => {
    const expectText = await page.$('.todoapp>.header>.h1').then((el) => el!.text())
    expect(expectText).toBe('Todos')
  })

  it('add todo', async () => {
    const text = 'add 1'
    page
      .$('.new-todo')
      .then((el) => (el as ElementExt).input(text))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await page.waitFor('.add-btn')
    await page.$('.add-btn').then((el) => el!.tap())
    const last = await page.$$('.todo-list>.todo').then((els: ElementExt[]) => els[els.length - 1])
    const expectText = await last.$('.view>label').then((el) => el!.text())
    expect(expectText).toBe(text)
  })

  it('edit todo', async () => {
    const editText = 'edit text'
    const last = await page.$$('.todo-list>.todo').then((els: ElementExt[]) => els[els.length - 1])
    await last.$('.view>label').then((el) => el!.longpress())
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.edit'))
    )
    await last
      .$('.edit')
      .then((el) => (el as ElementExt).input(editText))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await last.$('.confirm').then((el) => el!.tap())
    const expectText = await last.$('.view>label').then((el) => el!.text())
    expect(expectText).toBe(editText)
  })

  it('edit todo esc', async () => {
    const last = await page.$$('.todo-list>.todo').then((els: ElementExt[]) => els[els.length - 1])
    const oldText = await last.$('.view>label').then((el) => el!.text())
    await last.$('.view>label').then((el) => el!.longpress())
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.edit'))
    )
    await last
      .$('.edit')
      .then((el) => (el as ElementExt).input('will cancel text'))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await last.$('.cancel').then((el) => el!.tap())
    const expectText = await last.$('.view>label').then((el) => el!.text())
    expect(expectText).toBe(oldText)
  })

  it('delete todo', async () => {
    const LengthBefore = await page.$$('.todo-list>.todo').then((els) => els.length)
    const last = await page.$$('.todo-list>.todo').then((els: ElementExt[]) => els[els.length - 1])
    await last.$('.destroy').then((el) => el!.tap())
    const LengthAfter = await page.$$('.todo-list>.todo').then((els) => els.length)
    expect(LengthBefore - LengthAfter).toBe(1)
  })

  afterAll(async () => {
    await program.close()
  })
})
