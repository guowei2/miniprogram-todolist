/** @type {import('cz-git').UserConfig} */
module.exports = {
  prompt: {
    useEmoji: true,
  },
  extends: ["@commitlint/config-conventional"],
};
